package com.banyanhills.canopy.campaign.repository

import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository

import com.banyanhills.canopy.campaign.domain.Segment

interface SegmentRepository extends PagingAndSortingRepository<Segment, Long>, JpaSpecificationExecutor<Segment> {
	
	@Query("select max(sortOrder) + 1 from Segment s where s.campaign._bid = ?1")
	Integer getNextSortOrder(Long campaign_bid);
	
	@Query("select _bid from Segment s where s.campaign._bid = ?1 and s.sortOrder < ?2 order by s.sortOrder desc")
	List<Long> getPreviouSegmentBids(Long campaign_bid, Integer sortOrder)
	
	@Query("select _bid from Segment s where s.campaign._bid = ?1 and s.sortOrder > ?2 order by s.sortOrder")
	List<Long> getNextSegmentBids(Long campaign_bid, Integer sortOrder)
}
