package com.banyanhills.canopy.campaign.repository

import org.springframework.data.repository.PagingAndSortingRepository

import com.banyanhills.canopy.campaign.domain.MediaType
import com.banyanhills.canopy.campaign.domain.RequestContent

interface RequestContentRepository extends PagingAndSortingRepository<RequestContent, Long> {
	List<RequestContent> findByMediaUrlCheckSumAndMediaCheckSumAndMediaTypeAndSuccess(String mediaUrlCheckSum, String mediaCheckSum, MediaType mediaType, boolean success);
	
}
