package com.banyanhills.canopy.campaign.repository

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.banyanhills.canopy.campaign.domain.Content;

interface ContentRepository extends PagingAndSortingRepository<Content, Long>, JpaSpecificationExecutor<Content> {

}
