package com.banyanhills.canopy.campaign.repository

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository

import com.banyanhills.canopy.campaign.domain.Campaign

interface CampaignRepository extends PagingAndSortingRepository<Campaign, Long>, JpaSpecificationExecutor<Campaign>
{
	List<Campaign> findByCampaignId(String campaignId);
	
	List<Campaign> findBy_bidIn(List<Long> idList)
	
}
