package com.banyanhills.canopy.campaign.repository

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.banyanhills.canopy.campaign.domain.Campaign;
import com.banyanhills.canopy.campaign.domain.StreamRequest;

interface StreamRequestRepository extends PagingAndSortingRepository<StreamRequest, Long>, JpaSpecificationExecutor<StreamRequest> {
	StreamRequest findByRequestId(String requestId)
	
}
