package com.banyanhills.canopy.campaign

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.PropertySource
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.filter.CommonsRequestLoggingFilter

import com.banyanhills.ext.attributes.config.EnableCanopyAttributes
import com.banyanhills.tenant.annotation.EnableCanopyTenant

@SpringBootApplication
@EnableCanopyTenant
@EnableCanopyAttributes
@PropertySource('${CANOPY_DEFAULT_CONFIG}')
@EnableAsync
class CanopyCampaignService {
	static void main(String[] args) {
		SpringApplication.run CanopyCampaignService, args
	}
	
	@Bean
	public CommonsRequestLoggingFilter requestLoggingFilter() {
		CommonsRequestLoggingFilter crlf = new CommonsRequestLoggingFilter();
		crlf.setMaxPayloadLength(Integer.MAX_VALUE);
		crlf.setIncludeClientInfo(true);
		crlf.setIncludeQueryString(true);
		crlf.setIncludePayload(true);
		return crlf;
	}
}
