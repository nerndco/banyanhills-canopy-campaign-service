package com.banyanhills.canopy.campaign.service;

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import com.banyanhills.canopy.campaign.domain.Content


public class RangeParser {

	public static Range parseRange(HttpServletRequest request,
			HttpServletResponse response, Content content) throws IOException {
		long fileLength = content.size;

		if (fileLength == 0) {
			return null;
		}

		String rangeHeader = request.getHeader("Range");
		if (rangeHeader == null) {
			return null;
		}

		// bytes is the only range unit supported (and I don't see the point of
		// adding new ones)
		if (!rangeHeader.startsWith("bytes")) {
			response.addHeader("Content-Range", "bytes */" + fileLength);
			response.sendError(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE);
			return null;
		}

		rangeHeader = rangeHeader.substring(6);

		StringTokenizer commaTokenizer = new StringTokenizer(rangeHeader, ",");
		// Parsing the range list
		while (commaTokenizer.hasMoreTokens()) {
			String rangeDefinition = commaTokenizer.nextToken().trim();
			Range currentRange = new Range();
			currentRange.length = fileLength;

			int dashPos = rangeDefinition.indexOf('-');
			if (dashPos == -1) {
				response.addHeader("Content-Range", "bytes */" + fileLength);
				response.sendError(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE);
				return null;
			}

			if (dashPos == 0) {
				try {
					long offset = Long.parseLong(rangeDefinition);
					currentRange.start = fileLength + offset;
					currentRange.end = fileLength - 1;
				} catch (NumberFormatException e) {
					response.addHeader("Content-Range", "bytes */" + fileLength);
					response.sendError(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE);
					return null;
				}
			} else {
				try {
					currentRange.start = Long.parseLong(rangeDefinition
							.substring(0, dashPos));
					if (dashPos < rangeDefinition.length() - 1) {
						currentRange.end = Long.parseLong(rangeDefinition
								.substring(dashPos + 1,
										rangeDefinition.length()));
					} else {
						currentRange.end = fileLength - 1;
					}
				} catch (NumberFormatException e) {
					response.addHeader("Content-Range", "bytes */" + fileLength);
					response.sendError(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE);
					return null;
				}
			}

			if (!currentRange.validate()) {
				response.addHeader("Content-Range", "bytes " + "*" + "/"
						+ fileLength);
				response.sendError(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE);
				return null;
			}
			
			return currentRange;
		}

		return null;
	}

}
