package com.banyanhills.canopy.campaign.service

import groovy.util.logging.Slf4j

import java.util.Map;
import java.util.regex.Matcher
import java.util.regex.Pattern

import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

import com.banyanhills.canopy.campaign.domain.Content
import com.banyanhills.canopy.campaign.domain.MediaType

@Component
@Slf4j
class VideoProcessor 
{
	@Value('${content.ffmpeg}')
	String ffmpegApp;

	@Value('${content.qt-faststart}')
	String faststart;
	
	@Value('${content.ffprobe}')
	String ffprobeApp;
	
	@Value('${content.wmencoder:null}')
	String wmencoder;
	
	@Value('${content.thumbnail.video.hour}')
	int thumbVideoHour

	@Value('${content.thumbnail.video.minute}')
	int thumbVideoMinute

	@Value('${content.thumbnail.video.second}')
	int thumbVideoSecond
	
	@Value('${content.directory}')
	String contentDirectory;
	
	@Autowired
	ImageProcessor imageProcessor;
	
	
	public String process(String... cmd) {
		log.info("Video process starts, " + cmd);
		ProcessBuilder processBuilder = new ProcessBuilder(cmd);
		processBuilder.redirectErrorStream = true;
		Process process = processBuilder.start();
		consumeStream(process.getInputStream());
		process.waitFor();
		String sb = consumeStream(process.getErrorStream());
		log.info("Video process finished, " + cmd);
		return sb;
	}
	
	public void generateThumbnails(Content content)
					throws IOException, InterruptedException {
		Map<String, Object> fieldMap = new HashMap<String, Object>();
		ProcessBuilder processBuilder;
	
		String filename = contentDirectory + File.separator + content._bid;
		String outputFileName = contentDirectory + File.separator + content._bid + ".jpg";
		String sb = process(ffmpegApp, "-y", "-i", filename,
				"-vframes", "1", "-ss", thumbVideoHour + ":" + thumbVideoMinute + ":" + thumbVideoSecond, "-f",
				"mjpeg", "-an", outputFileName);
	
		Pattern pattern = Pattern.compile("Duration: (.*?),");
	
		Matcher matcher = pattern.matcher(sb);
	
		if (matcher.find()) {
			String time = matcher.group(1);
			time = StringUtils.substringBeforeLast(time, ".");
			String[] times = StringUtils.split(time, ":");
			Long duration = 0;
			duration += (times[0] as Long) * 60 * 60;
			duration += (times[1] as Long) * 60;
			duration += (times[2] as Long);
			content.duration = duration;
		}
		
		imageProcessor.generateThumbnails(content, outputFileName);
		new File(outputFileName).delete();
	}
					
	protected String consumeStream(InputStream is) {
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line = br.readLine();
			while (line != null) {
				log.info(line)
				sb.append(line);
				line = br.readLine();

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}
	
	private String secondsToTimeString(int totalSeconds) {
		int hours = totalSeconds / 3600;
		int minutes = (totalSeconds % 3600) / 60;
		int seconds = totalSeconds % 60;
			
		return String.format("%02d:%02d:%02d", hours, minutes, seconds);
	}
	
	public void clip(File input, File output, String vcodec, Integer startSecond, Integer endSecond, Integer lengthSeconds, Long width, Long height, MediaType mediaType) 
	{
		if (startSecond == null) {
			startSecond = 0;
		}
		String startString = secondsToTimeString(startSecond)
		
		String lengthString = null;
		if (lengthSeconds != null) {
			lengthString = secondsToTimeString(lengthSeconds);
		}
		else if (endSecond != null) {
			lengthSeconds = endSecond - startSecond + 1
			lengthString = secondsToTimeString(lengthSeconds);
		}
		
		if (width%2 != 0) width = width - 1;
		if (height%2 != 0) height = height - 1;
		
		if (lengthString != null) {
			if (width != 0 && height != 0) {
				process(ffmpegApp, "-loglevel", "warning", "-y", "-i", input.getAbsolutePath(),
					"-ss", startString, "-t", lengthString,
					"-vcodec", vcodec, "-acodec", "wmav2", "-s", "${width}x${height}",
					output.getAbsolutePath());
			}
			else {
				process(ffmpegApp, "-loglevel", "warning", "-y", "-i", input.getAbsolutePath(),
					"-ss", startString, "-t", lengthString,
					"-vcodec", vcodec, "-acodec", "wmav2",
					output.getAbsolutePath());
			}
		}
		else {
			if (width != 0 && height != 0) {
				process(ffmpegApp, "-loglevel", "warning", "-y", "-i", input.getAbsolutePath(),
					"-ss", startString,
					"-vcodec", vcodec, "-acodec", "wmav2", "-s", "${width}x${height}",
					output.getAbsolutePath());
			}
			else {
				process(ffmpegApp, "-loglevel", "warning", "-y", "-i", input.getAbsolutePath(),
					"-ss", startString,
					"-vcodec", vcodec, "-acodec", "wmav2",
					output.getAbsolutePath());
			}
		}
		
		if (output.exists()) {
			if (mediaType == MediaType.wmv3) {
				wmEncoder(output);
			}
		}
	}
	
	public void imageToVideo(File input, File output, String vcodec, Long width, Long height, Integer length, MediaType mediaType) {
		String lengthString = secondsToTimeString(length);
		
		if (width%2 != 0) width = width - 1;
		if (height%2 != 0) height = height - 1;
		
		process(ffmpegApp,
			"-loglevel", "warning", "-f", "lavfi", "-i", "aevalsrc=0",
			"-loop", "1", "-framerate", "30", 
			"-y", "-i", input.getAbsolutePath(),
			"-t", lengthString,
			"-vcodec", vcodec, "-acodec", "wmav2", "-s", "${width}x${height}",
			output.getAbsolutePath());
		

		if (mediaType == MediaType.wmv3) {
			wmEncoder(output, "100000", "15");
		}
	}
	
	private void wmEncoder(File output, String bitrate = "250000", String framerate = "30") 
	{
		File wmv3File = new File(output.getAbsolutePath() + ".new");
		process("cscript",
			wmencoder,
			"-input", output.getAbsolutePath(),
			"-output", wmv3File.getAbsolutePath(),
			"-v_bitrate", bitrate, "-v_width", "640", "-v_height", "240", "-v_framerate", framerate);
		output.delete();
		if (wmv3File.exists()) {
			wmv3File.renameTo(output.getAbsolutePath());
		}
	}
	
	private Map parseParameter(String adUrl) {
		Map parameters = [:];
		if (StringUtils.isNotEmpty(adUrl)) {
			String[] paramValues = StringUtils.split(adUrl, "&");
			paramValues.each { String param ->
				if (StringUtils.isNotEmpty(param)) {
					String[] values = StringUtils.split(param, "=");
					if (values.length == 2) {
						parameters.put(values[0], values[1]);
					}
				}
			}
		}
		return parameters;
	}
	
}
