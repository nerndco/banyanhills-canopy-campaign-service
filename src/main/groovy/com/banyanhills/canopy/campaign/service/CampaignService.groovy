package com.banyanhills.canopy.campaign.service

import groovy.util.logging.Slf4j

import java.util.concurrent.ThreadPoolExecutor

import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import org.springframework.stereotype.Service

import com.banyanhills.canopy.campaign.domain.Campaign
import com.banyanhills.canopy.campaign.exception.ConflictException
import com.banyanhills.canopy.campaign.exception.NotFoundException
import com.banyanhills.canopy.campaign.repository.CampaignRepository
import com.banyanhills.canopy.service.util.search.KendoSpecification
import com.banyanhills.canopy.service.util.search.SearchCriteria
import com.banyanhills.ext.attributes.service.AttributeService

@Service
@Slf4j
class CampaignService implements InitializingBean   
{
	static CampaignService instance;
	
	@Autowired
	CampaignRepository campaignRepository
	
	@Autowired
	ThreadPoolTaskExecutor campaignTaskExecutor;
	
	@Override
	void afterPropertiesSet() throws Exception {
		instance = this
	}
	
	Page<Campaign> search(Pageable pageable, SearchCriteria searchCriteria) {
		searchCriteria = AttributeService.instance.applySearchCriteria(Campaign.class, searchCriteria)
		return campaignRepository.findAll(new KendoSpecification<Campaign>(searchCriteria), pageable)
	}
	
	List<Campaign> getFromList(List<Long> idList) {
		List<Campaign> list = new ArrayList<Campaign>()
		list.addAll(campaignRepository.findBy_bidIn(idList))
		return list
	}

	Campaign get(Long id) {
		return campaignRepository.findOne(id);
	}
	
	Campaign create(Campaign campaign) {
		log.debug("Creating Campaign: ${campaign}")
		if (campaign._bid == null) {
			campaign = campaignRepository.save(campaign)
		} else {
			log.debug("Could not create Campaign (campaign exists): ${campaign}")
			throw new ConflictException("Campaign Exists")
		}
		return campaign
	}

	Campaign update(Campaign campaign) {
		Campaign existing = campaignRepository.findOne(campaign._bid)

		if (existing == null) {
			throw new NotFoundException("Campaign not existing: ${campaign._bid}")
		}
		
		existing.name = campaign.name;
		existing.description = campaign.description;
		existing.startDate = campaign.startDate;
		existing.endDate = campaign.endDate;

		existing = campaignRepository.save(existing)

		return existing
	}

	void delete(Long id) {
		Campaign campaign = campaignRepository.findOne(id as Long)

		if (campaign != null) {
			campaignRepository.delete(campaign)
		}
	}
	
	def status() {
		ThreadPoolExecutor executor = campaignTaskExecutor.getThreadPoolExecutor()
		
		return [
			"activeCount": executor.getActiveCount(),
			"completedTaskCount": executor.getCompletedTaskCount(),
			"corePoolSize":  executor.getCorePoolSize(),
			"queueSize": executor.getQueue().size(),
			"poolSize": executor.getPoolSize(),
			"taskCount": executor.getTaskCount(),
			"maximumPoolSize": executor.getMaximumPoolSize(),
			"largestPoolSize": executor.getLargestPoolSize()
		]
	}
}
