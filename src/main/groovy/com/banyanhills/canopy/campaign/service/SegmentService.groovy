package com.banyanhills.canopy.campaign.service

import groovy.util.logging.Slf4j

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import com.banyanhills.canopy.campaign.domain.Campaign
import com.banyanhills.canopy.campaign.domain.Segment
import com.banyanhills.canopy.campaign.domain.StreamRequest
import com.banyanhills.canopy.campaign.repository.CampaignRepository
import com.banyanhills.canopy.campaign.repository.SegmentRepository

@Service
@Slf4j
class SegmentService 
{
	@Autowired
	CampaignRepository campaignRepository;
	
	@Autowired
	SegmentRepository segmentRepository;
	
	@Autowired
	StreamService streamService;
	
	List<Segment> getAll(Long campaign_bid) {
		Campaign campaign = campaignRepository.findOne(campaign_bid);
		if (campaign != null) {
			List<Segment> segments = campaign.segments;
			segments.sort {it.sortOrder}
			return segments;
		}
		else {
			return [];
		}
	}
	
	Segment create(Segment segment) 
	{	Integer nextSortOrder = segmentRepository.getNextSortOrder(segment.campaign_bid);
		segment.sortOrder = nextSortOrder ? nextSortOrder : 1
		segmentRepository.save(segment);
		StreamRequest streamRequest = new StreamRequest(segment:segment);
		streamService.generateStreamContent(streamRequest);
		return segment;
	}
	
	void move(List<Segment> segments) 
	{
		segments.each { Segment segmentToMove ->
			Segment segment = segmentRepository.findOne(segmentToMove._bid);
			if (segment != null) {
				segment.sortOrder = segmentToMove.sortOrder;
				segmentRepository.save(segment);
			}
		}
	}
	
	void moveUp(Long segment_bid)
	{	
		Segment segment = segmentRepository.findOne(segment_bid);
		if (segment != null && segment.sortOrder > 0) 
		{
			List<Long> bids = segmentRepository.getPreviouSegmentBids(segment.campaign_bid, segment.sortOrder);
			if (bids.size() > 0) {
				Long previouSegmentBid = bids[0];
				Segment previouSegment = segmentRepository.findOne(previouSegmentBid);
				if (previouSegment != null) {
					int previouSegmentSortOrder = previouSegment.sortOrder;
					previouSegment.sortOrder = segment.sortOrder;
					segment.sortOrder = previouSegmentSortOrder;
					segmentRepository.save(segment);
					segmentRepository.save(previouSegment);
				}
				
			}
			
		}
	}
	
	void moveDown(Long segment_bid)
	{
		Segment segment = segmentRepository.findOne(segment_bid);
		if (segment != null && segment.sortOrder > 0)
		{
			List<Long> bids = segmentRepository.getNextSegmentBids(segment.campaign_bid, segment.sortOrder);
			if (bids.size() > 0) {
				Long nextBid = bids[0];
				Segment nextSegment = segmentRepository.findOne(nextBid);
				if (nextSegment != null) {
					int nextSegmentSortOrder = nextSegment.sortOrder;
					nextSegment.sortOrder = segment.sortOrder;
					segment.sortOrder = nextSegmentSortOrder;
					segmentRepository.save(segment);
					segmentRepository.save(nextSegment);
				}
			}
		}
	}
	
	Segment update(Segment segment) {
		Segment existing = segmentRepository.findOne(segment._bid);
		if (existing != null) {
			existing.startSecond = segment.startSecond;
			existing.endSecond = segment.endSecond;
			existing.lengthSeconds = segment.lengthSeconds;
			existing.mediaType = segment.mediaType
			existing.content = segment.content;
			segmentRepository.save(existing);
			StreamRequest streamRequest = new StreamRequest(segment:existing);
			streamService.deleteStreamContent(streamRequest);
			streamService.generateStreamContent(streamRequest);
	
		}
		
		return existing
		
	}
		
	void delete(Long segment_bid) {
		Segment segment = segmentRepository.findOne(segment_bid);
		if (segment != null) {
			StreamRequest streamRequest = new StreamRequest(segment:segment);
			streamService.deleteStreamContent(streamRequest);
			segment.campaign.updated = new Date();
			campaignRepository.save(segment.campaign);
			segmentRepository.delete(segment_bid);
		}
	}
		
}

