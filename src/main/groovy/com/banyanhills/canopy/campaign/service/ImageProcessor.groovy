package com.banyanhills.canopy.campaign.service

import java.awt.image.BufferedImage

import javax.imageio.ImageIO

import org.apache.commons.lang3.StringUtils
import org.imgscalr.Scalr
import org.imgscalr.Scalr.Method
import org.imgscalr.Scalr.Mode
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

import com.banyanhills.canopy.campaign.domain.Content

@Component
class ImageProcessor 
{
	@Value('${content.directory}')
	String contentDirectory;
	
	@Value('${content.thumbnail.image.width}')
	int thumbnailImageWidth
	
	public void generateThumbnails(Content content, String filename = null) 
	{
		String ext;
		if (StringUtils.isNotEmpty(filename)) {
			ext = StringUtils.substringAfterLast(filename, ".");
		}
		else {
			filename = contentDirectory + File.separator + content._bid
			ext = content.ext;
		}
		
		BufferedImage originalImage = ImageIO.read(new File(filename))
		
		content.width = originalImage.width;
		content.height = originalImage.height;
		
		BufferedImage thumbImg = Scalr.resize(originalImage, Method.QUALITY, Mode.FIT_TO_WIDTH, thumbnailImageWidth, originalImage.height, Scalr.OP_ANTIALIAS);
		
		content.thumbnailWidth = thumbImg.width;
		content.thumbnailHeight = thumbImg.height;

		ImageIO.write(thumbImg, ext.toUpperCase(), new File(contentDirectory + File.separator + content.thumbnailFileName));
	}
	
}
