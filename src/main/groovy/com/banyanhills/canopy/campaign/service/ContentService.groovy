package com.banyanhills.canopy.campaign.service

import groovy.util.logging.Slf4j

import java.security.MessageDigest

import javax.servlet.ServletOutputStream
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile

import com.banyanhills.canopy.campaign.domain.Content
import com.banyanhills.canopy.campaign.exception.BadRequestException
import com.banyanhills.canopy.campaign.exception.NotFoundException
import com.banyanhills.canopy.campaign.repository.ContentRepository
import com.banyanhills.canopy.service.util.search.KendoSpecification
import com.banyanhills.canopy.service.util.search.SearchCriteria
import com.banyanhills.ext.attributes.service.AttributeService

@Service
@Slf4j
class ContentService implements InitializingBean  
{
	static ContentService instance;

	@Autowired
	ContentRepository contentRepository
	
	@Autowired
	VideoProcessor videoProcessor
	
	@Autowired
	ImageProcessor imageProcessor
	
	@Value('${content.directory}')
	String contentDirectory;
	
	@Value('${canopy.campaign.service.url}')
	String canopyCampaignServiceUrl
	
	@Override
	void afterPropertiesSet() throws Exception {
		instance = this
	}
	
	Page<Content> search(Pageable pageable, SearchCriteria searchCriteria) {
		searchCriteria = AttributeService.instance.applySearchCriteria(Content.class, searchCriteria)
		return contentRepository.findAll(new KendoSpecification<Content>(searchCriteria), pageable)
	}
	
	Content get(Long id) {
		return contentRepository.findOne(id)
	}
		
	
	Content update(Content content) {
		Content existing = contentRepository.findOne(content._bid)

		if (existing == null) {
			throw new NotFoundException("Content not existing: ${content._bid}")
		}

		existing.name = content.name;
		existing.description = content.description;
		existing = contentRepository.save(existing)
		
		return existing
	}
	
	void delete(Long id) {
		Content existing = contentRepository.findOne(id)
		
		if (existing == null) {
			throw new NotFoundException("Content not existing: ${id}")
		}
		
		contentRepository.delete(id);
		new File(contentDirectory + File.separator + existing._bid).delete();
		new File(contentDirectory + File.separator + existing.thumbnailFileName).delete();
	}
		
	
	Content save(MultipartFile multipartFile) 
	{
		log.debug("Creating Content")
		
		String contentType = multipartFile.getContentType();
		if (contentType == null)
			contentType = "";
			
		String fileName = multipartFile.getOriginalFilename();
		if (StringUtils.isEmpty(fileName)) {
			fileName = multipartFile.getName();
		}
		
		String ext = StringUtils.substringAfterLast(fileName, ".");
		Content content = new Content();
		content.contentType = contentType;
		content.name = fileName;
		content.description = fileName;
		content.fileName = fileName;
		content.ext = ext;
		content.size = multipartFile.getBytes().length;
		if (!content.isValid()) {
			throw new BadRequestException("Invalid Content");
		}
		Content savedContent = contentRepository.save(content);
	

		byte[] fileContents = multipartFile.getBytes();
		String filepath = contentDirectory + File.separator + savedContent._bid;
		BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filepath)));
		stream.write(fileContents);
		stream.close();
		
		generateThumbnails(savedContent);

		MessageDigest md5 = MessageDigest.getInstance("MD5");
		md5.update(fileContents);
		BigInteger hash = new BigInteger(1, md5.digest());
		savedContent.checksum = hash.toString(16);
		savedContent = contentRepository.save(savedContent);
		
		return savedContent;
	}
	
	private void generateThumbnails(Content content) 
	{
		if (content.isVideo()) {
			videoProcessor.generateThumbnails(content);
		}
		else {
			imageProcessor.generateThumbnails(content);
		}
	}
	
	String getImageLink(Content content)
	{
		if (content != null && StringUtils.isNotEmpty(content.fileName)) {
			return canopyCampaignServiceUrl + "/v1/contents/image/${content._bid}?ts=" + System.currentTimeMillis()
		}
		return null;
	}
	
	String getStreamLink(Content content)
	{
		if (content != null && StringUtils.isNotEmpty(content.fileName)) {
			return canopyCampaignServiceUrl + "/v1/contents/stream/${content._bid}?ts=" + System.currentTimeMillis()
		}
		return null;
	}
	
	String getThumbnailLink(Content content) 
	{
		if (content != null && StringUtils.isNotEmpty(content.thumbnailFileName)) {
			return canopyCampaignServiceUrl + "/v1/contents/image/thumbnail/${content._bid}?ts=" + System.currentTimeMillis()
		}
		return null;
	}	
	
	ResponseEntity<byte[]> getImage(Long id)
	{
		Content content = contentRepository.findOne(id);
		if (content != null && StringUtils.isNotEmpty(content.fileName)) {
			File file = new File(contentDirectory + File.separator + content._bid);
			if (file.exists()) {
				byte[] imageByte = file.getBytes();
				
				HttpHeaders headers = new HttpHeaders();
				headers.set("content-type", content.contentType);
				headers.setContentLength(imageByte.length);
				headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
				ResponseEntity<byte[]> responseEntity = new ResponseEntity<byte[]>(imageByte, headers, HttpStatus.OK);
				return responseEntity;
			}
			else {
				return null;
			}
		}
		else {
			return null;
		}
	}
	
	
	ResponseEntity<byte[]> getThumbnail(Long id)
	{
		Content content = contentRepository.findOne(id);
		if (content != null && StringUtils.isNotEmpty(content.thumbnailFileName)) {
			File file = new File(contentDirectory + File.separator + content.thumbnailFileName);
			if (file.exists()) {
				byte[] imageByte = file.getBytes();
				
				HttpHeaders headers = new HttpHeaders();
				headers.set("content-type", content.thumbnailContentType);
				headers.setContentLength(imageByte.length);
				headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
				ResponseEntity<byte[]> responseEntity = new ResponseEntity<byte[]>(imageByte, headers, HttpStatus.OK);
				return responseEntity;
			}
			else {
				return null;
			}
		}
		else {
			return null;
		}
	}
	
	
	void download(Long id, HttpServletRequest request, HttpServletResponse response) {
		Content content = contentRepository.findOne(id);
		if (content != null && StringUtils.isNotEmpty(content.fileName)) {
			File file = new File(contentDirectory + File.separator + content._bid);
			if (file.exists()) {
				response.setHeader("Content-Type", content.contentType);
				response.setHeader("Content-Disposition", "attachment; filename=${content.fileName}");
				response.setHeader("X-FILENAME", content.fileName);
				response.setHeader("Cache-Control", "no-store, must-revalidate");
				response.setHeader("Content-Length", "" + content.size);
				
				FileInputStream fis = new FileInputStream(file);
				ServletOutputStream outputStream = response.getOutputStream();
				BufferedInputStream bis = new BufferedInputStream(fis);
				
				byte[] data = new byte[1024];
				long tmpLength = 0;
				while ((tmpLength = bis.read(data)) > 0) {
					outputStream.write(data, 0, (int)tmpLength);
				}
				fis.close();
			}
			
		}
		
	}
		
	
	void stream(Long id, HttpServletRequest request, HttpServletResponse response) {
		Content content = contentRepository.findOne(id);
		if (content != null && StringUtils.isNotEmpty(content.fileName)) {
			response.setHeader("Cache-Control", "no-store, must-revalidate");
			response.setHeader("Accept-Ranges", "bytes");
			
			File file = new File(contentDirectory + File.separator + content._bid);
			if (file.exists()) {
				FileInputStream fis = new FileInputStream(file);
				response.setContentType(content.contentType);
				
				ServletOutputStream outputStream = response.getOutputStream();
				
				Range range = RangeParser.parseRange(request, response, content);
				
				if (range == null) {
					response.setHeader("Content-Length", "" + content.size);
					BufferedInputStream bis = new BufferedInputStream(fis);
					byte[] data = new byte[1024];
					long tmpLength = 0;
					while ((tmpLength = bis.read(data)) > 0) {
						outputStream.write(data, 0, (int)tmpLength);
					}
				}
				else {
					response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
					response.setBufferSize(1024 * 16);
					
					response.addHeader("Content-Range", "bytes " + range.start
							+ "-" + range.end + "/" + range.length);
					long bytesToRead = range.end - range.start + 1;
					response.setHeader("content-length", String.valueOf(bytesToRead));
					
					BufferedInputStream bis = new BufferedInputStream(fis);
					bis.skip(range.start);
					
					byte[] data = new byte[1024];
					long tmpLength = 0;
					while ((tmpLength = bis.read(data)) > 0) {
						if (bytesToRead >= tmpLength) {
							outputStream.write(data, 0, (int)tmpLength);
							bytesToRead = bytesToRead - tmpLength;
						}
						else {
							outputStream.write(data, 0, (int)bytesToRead);
							bytesToRead = 0;
							break;
						}
					}
				}
				
				fis.close();
			}
			else {
				return;
			}
			
		}
		else {
			return;
		}
	}
	
}
