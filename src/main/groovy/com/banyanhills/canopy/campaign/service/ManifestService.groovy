package com.banyanhills.canopy.campaign.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

import com.banyanhills.canopy.campaign.domain.Campaign
import com.banyanhills.canopy.campaign.domain.Manifest
import com.banyanhills.canopy.campaign.domain.ManifestLine
import com.banyanhills.canopy.campaign.domain.Segment
import com.banyanhills.canopy.campaign.repository.CampaignRepository

@Service
class ManifestService {
	
	@Autowired
	SegmentService segmentService;
	
	@Autowired
	CampaignRepository campaignRepo;
	
	@Value('${canopy.campaign.service.url}')
	String canopyCampaignServiceUrl
	
	private boolean check(Long campaign_bid, Long timestamp) {
		boolean changed = false;
		if (timestamp == null) timestamp = 0;
		
		Campaign campaign = campaignRepo.findOne(campaign_bid);
		if (campaign != null) {
			if (campaign.updated.getTime() > timestamp) {
				changed = true;
			}
			
			if (!changed) {
				List<Segment> segments = campaign.segments;
				segments.each { Segment segment ->
					if (segment.updated.getTime() > timestamp) {
						changed = true;
					}
				}
			}
		}
		
		return changed;
	}
	
	Manifest get(String id, Long timestamp) {
		Long campaign_bid;
		if (id.isNumber()) {
			Campaign campaign = campaignRepo.findOne(id as Long);
			campaign_bid = campaign?._bid;
		}
		
		if (campaign_bid == null) {
			List<Campaign> campaigns = campaignRepo.findByCampaignId(id);
			if (campaigns.size() > 0) {
				campaign_bid = campaigns[0]?._bid;
			}
		}
		
		if (campaign_bid == null) {
			return null;
		}
		
		if (check(campaign_bid, timestamp)) {
			Campaign campaign = campaignRepo.findOne(campaign_bid);
			if (campaign != null) {
				Manifest manufest = new Manifest(name:campaign.name);
				List<Segment> segments = segmentService.getAll(campaign_bid);
				manufest.manifestLines = [];
				manufest.timestamp = System.currentTimeMillis();
				segments.each { Segment segment ->
					ManifestLine manifestLine = new ManifestLine();
					manifestLine.order = segment.sortOrder;
					manifestLine.remoteLocation = canopyCampaignServiceUrl + "/v1/campaigns/segments/stream/request/${segment._bid}";
					
					manufest.manifestLines.add(manifestLine);
				}
				
				return manufest;
			}
		}
		
		return null;
	}
}
