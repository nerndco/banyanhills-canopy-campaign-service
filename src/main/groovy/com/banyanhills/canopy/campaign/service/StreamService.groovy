package com.banyanhills.canopy.campaign.service

import groovy.util.logging.Slf4j

import javax.servlet.ServletOutputStream
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.apache.commons.lang3.StringUtils
import org.springframework.beans.BeansException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.stereotype.Component

import com.banyanhills.canopy.campaign.domain.RequestContent
import com.banyanhills.canopy.campaign.domain.Segment
import com.banyanhills.canopy.campaign.domain.SegmentType
import com.banyanhills.canopy.campaign.domain.StreamRequest
import com.banyanhills.canopy.campaign.domain.StreamRequestStatus
import com.banyanhills.canopy.campaign.exception.NotFoundException
import com.banyanhills.canopy.campaign.repository.SegmentRepository
import com.banyanhills.canopy.campaign.repository.StreamRequestRepository
import com.banyanhills.canopy.campaign.stream.StreamProcessor

@Component
@Slf4j
class StreamService implements ApplicationContextAware
{
	@Autowired
	SegmentRepository segmentRepository;
	
	@Autowired
	StreamRequestRepository streamRequestRepository;
	
	ApplicationContext applicationContext;
	
	@Value('${stream.directory}')
	String streamDirectory;
	
	@Value('${canopy.campaign.service.url}')
	String canopyCampaignServiceUrl
	
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException 
	{
		this.applicationContext = applicationContext;
	}
	
	StreamRequest requestStream(Long id, String requestId, String stream, String host, String ip, String mac, HttpServletRequest request) 
	{
		log.info("Request Stream for segmentId:${id}, requestId:${requestId}, stream:${stream}, host:${host}, ip:${ip}");
		Segment segment = segmentRepository.findOne(id);
		StreamRequest streamRequest;
		if (requestId != null) {
			streamRequest = streamRequestRepository.findByRequestId(requestId)
			streamRequest.status = StreamRequestStatus.Processing
			if (StringUtils.isNotEmpty(stream)) {
				streamRequest.stream = stream;
			}
			if (StringUtils.isNotEmpty(host)) {
				streamRequest.host = host;
			}
			if (StringUtils.isNotEmpty(ip)) {
				streamRequest.ip = ip;
			}
			if (StringUtils.isNotEmpty(mac)) {
				streamRequest.mac = mac;
			}
			streamRequest = streamRequestRepository.save(streamRequest);
		}
		else {
			requestId = UUID.randomUUID().toString()
			streamRequest = new StreamRequest();
			streamRequest.status = StreamRequestStatus.Processing
			streamRequest.requestId = requestId;
			streamRequest.segment = segment;
			streamRequest.stream = stream;
			streamRequest.host = host;
			streamRequest.ip = ip;
			streamRequest.mac = mac;
			streamRequest = streamRequestRepository.save(streamRequest);
		}
		
		streamRequest.parameters = [:]
		request.getParameterNames().each {
			streamRequest.parameters.put(it, request.getParameter(it))
		}

		streamRequest.remoteLocation = canopyCampaignServiceUrl + "/v1/campaigns/segments/stream/download/${requestId}";
		if (segment.type == SegmentType.Ad) {
			streamRequest.repeatInterval = segment.requestInterval;
			streamRequest.repeatLocation = canopyCampaignServiceUrl + "/v1/campaigns/segments/stream/request/${id}?requestId=${requestId}";
		}
		else {
			streamRequest.repeatInterval = 0;
			streamRequest.repeatLocation = null;
		}
		
		generateStreamContent(streamRequest);
		log.info("Request Stream success for segmentId:${id}, requestId:${requestId}, stream:${stream}, host:${host}, ip:${ip}");
		return streamRequest;
	}
	
	void playback(String requestId, HttpServletRequest request, HttpServletResponse response) {
		StreamRequest streamRequest = streamRequestRepository.findByRequestId(requestId);
		if (streamRequest != null && !(streamRequest.status in [StreamRequestStatus.Fail, StreamRequestStatus.Processing])) {
			StreamProcessor processor = getStreamProcessor(streamRequest.segment);
			File file = processor.get(streamRequest);
			if (file.exists()) {
				FileInputStream fis = new FileInputStream(file);
				response.setHeader("Cache-Control", "no-store, must-revalidate");
				response.setHeader("Accept-Ranges", "bytes");
				
				response.setHeader("Content-Type", "video/x-ms-wmv");
				
				ServletOutputStream outputStream = response.getOutputStream();
				
				Range range = RangeParser.parseRange(request, response, streamRequest.segment.content);
				
				if (range == null) {
					response.setHeader("Content-Length", "" + file.size());
					BufferedInputStream bis = new BufferedInputStream(fis);
					byte[] data = new byte[1024];
					long tmpLength = 0;
					while ((tmpLength = bis.read(data)) > 0) {
						outputStream.write(data, 0, (int)tmpLength);
					}
				}
				else {
					response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
					response.setBufferSize(1024 * 16);
					
					response.addHeader("Content-Range", "bytes " + range.start
							+ "-" + range.end + "/" + range.length);
					long bytesToRead = range.end - range.start + 1;
					response.setHeader("content-length", String.valueOf(bytesToRead));
					
					BufferedInputStream bis = new BufferedInputStream(fis);
					bis.skip(range.start);
					
					byte[] data = new byte[1024];
					long tmpLength = 0;
					while ((tmpLength = bis.read(data)) > 0) {
						if (bytesToRead >= tmpLength) {
							outputStream.write(data, 0, (int)tmpLength);
							bytesToRead = bytesToRead - tmpLength;
						}
						else {
							outputStream.write(data, 0, (int)bytesToRead);
							bytesToRead = 0;
							break;
						}
					}
				}
				
				fis.close();
			}
			else {
				throw new NotFoundException("Stream Request ${requestId} Not Found")
			}
		}
		else {
			throw new NotFoundException("Stream Request ${requestId} Not Found")
		}
	}
		
	
	
	void download(String requestId, HttpServletRequest request, HttpServletResponse response) {
		log.info("Download Stream for requestId:${requestId}");
		StreamRequest streamRequest = streamRequestRepository.findByRequestId(requestId);
		if (streamRequest != null && !(streamRequest.status in [StreamRequestStatus.Processing])) {
			if (streamRequest.status == StreamRequestStatus.Ready || streamRequest.status == StreamRequestStatus.Downloaded) {
				StreamProcessor processor = getStreamProcessor(streamRequest.segment);
				File file = processor.get(streamRequest);
				log.info("Download content from " + file);
				if (file.exists()) {
					response.setHeader("Content-Type", "video/" + streamRequest.segment.mediaType);
					response.setHeader("Content-Disposition", "attachment; filename=${file.getName()}");
					response.setHeader("X-FILENAME", file.getName());
					response.setHeader("Cache-Control", "no-store, must-revalidate");
					response.setHeader("Content-Length", "" + file.length());
					
					FileInputStream fis = new FileInputStream(file);
					ServletOutputStream outputStream = response.getOutputStream();
					BufferedInputStream bis = new BufferedInputStream(fis);
					
					byte[] data = new byte[1024];
					long tmpLength = 0;
					while ((tmpLength = bis.read(data)) > 0) {
						outputStream.write(data, 0, (int)tmpLength);
					}
					fis.close();
					
					if (streamRequest.status == StreamRequestStatus.Ready) {
						streamRequest.status = StreamRequestStatus.Downloaded;
						streamRequestRepository.save(streamRequest);
					}
				}
				else {
					throw new NotFoundException("Stream Request ${requestId} Not Found")
				}
			}
			else if (streamRequest.status == StreamRequestStatus.Fail) {
				StreamProcessor processor = getStreamProcessor(streamRequest.segment);
				File file = processor.get(streamRequest);
				log.info("Download content from " + file);
				if (file.exists()) {
					response.setHeader("Content-Type", "video/" + streamRequest.segment.mediaType);
					response.setHeader("Content-Disposition", "attachment; filename=${file.getName()}");
					response.setHeader("X-FILENAME", file.getName());
					response.setHeader("Cache-Control", "no-store, must-revalidate");
					response.setHeader("Content-Length", "" + file.length());
					
					FileInputStream fis = new FileInputStream(file);
					ServletOutputStream outputStream = response.getOutputStream();
					BufferedInputStream bis = new BufferedInputStream(fis);
					
					byte[] data = new byte[1024];
					long tmpLength = 0;
					while ((tmpLength = bis.read(data)) > 0) {
						outputStream.write(data, 0, (int)tmpLength);
					}
					fis.close();
				}
				else {
					String fileName = requestId + "." + streamRequest.segment.mediaType;
					response.setHeader("Content-Type", "video/" + streamRequest.segment.mediaType);
					response.setHeader("Content-Disposition", "attachment; filename=${fileName}");
					response.setHeader("X-FILENAME", fileName);
					response.setHeader("Cache-Control", "no-store, must-revalidate");
					response.setHeader("Content-Length", "" + 0);
					byte[] data = new byte[0];
					ServletOutputStream outputStream = response.getOutputStream();
					outputStream.write(data, 0, 0);
				}
			}
		}
		else {
			throw new NotFoundException("Stream Request ${requestId} Not Found")
		}
		log.info("Download Stream success for requestId:${requestId}");
	}
		
	
	public void generateStreamContent(StreamRequest request) 
	{
		StreamProcessor processor = getStreamProcessor(request.segment);
		if (processor != null) {
			processor.generate(request);
		}
	}
	
	public void deleteStreamContent(StreamRequest request)
	{
		StreamProcessor processor = getStreamProcessor(request.segment);
		if (processor != null) {
			processor.delete(request);
		}
	}

	
	public void updateStreamRequestStatus(Long id, StreamRequestStatus status, RequestContent requestContent = null) {
		StreamRequest request = streamRequestRepository.findOne(id);
		if (request != null) {
			request.status = status;
			if (requestContent != null) {
				request.requestContent = requestContent;
			}
			streamRequestRepository.save(request);
		}
	}
	
	private StreamProcessor getStreamProcessor(Segment segment) {
		StreamProcessor streamProcessor;
		if (segment.type == SegmentType.Ad) {
			streamProcessor = applicationContext.getBean(segment.adType.processorBean)
		}
		else if (segment.type == SegmentType.Content && segment.content != null) {
			if (segment.content.isVideo()) {
				streamProcessor = applicationContext.getBean("videoStreamProcessor")
			}
			else {
				streamProcessor = applicationContext.getBean("imageStreamProcessor")
			}
		}
		return streamProcessor;
	}
}
