package com.banyanhills.canopy.campaign.config

import java.util.concurrent.Executor

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor

@Configuration
@EnableAsync
class AsyncConfig {

	@Value('${task.maxPoolSize:50}')
	int maxPoolSize;
	
	@Value('${task.corePoolSize:10}')
	int corePoolSize;
	
	@Value('${task.queueCapacity:1000}')
	int queueCapacity;
	
	@Bean(name = "campaignTaskExecutor")
	public ThreadPoolTaskExecutor threadPoolTaskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.corePoolSize = corePoolSize;
		executor.maxPoolSize = maxPoolSize;
		executor.queueCapacity = queueCapacity;
		return executor;
	}
	
}
