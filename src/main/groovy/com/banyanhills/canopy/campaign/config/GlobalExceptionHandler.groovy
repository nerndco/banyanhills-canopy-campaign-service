package com.banyanhills.canopy.campaign.config

import groovy.util.logging.Slf4j

import javax.servlet.http.HttpServletRequest

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

import com.banyanhills.canopy.campaign.exception.CampaignException

/**
 * Translate exceptions in REST friendly response.
 * 
 * Each custom exception should extend CustomerException in order to be handled by GlobalExceptionHandler
 * to send appropriate error message and response status. 
 * @author Arvind.Chauhan
 *
 */
@ControllerAdvice
@Slf4j
class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	@ExceptionHandler(Throwable.class)
	@ResponseBody
	ResponseEntity<Map> handleControllerException(HttpServletRequest req, Throwable ex) {
		if(ex instanceof CampaignException) {
			return new ResponseEntity<Map>([success:false, message:ex.getMessage()], ex.statusCode);
		}
		else {
			log.error(ex.getMessage(), ex)
			return new ResponseEntity<Map>([success:false, message:ex.getMessage()], HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
