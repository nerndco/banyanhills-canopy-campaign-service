package com.banyanhills.canopy.campaign.exception

import groovy.lang.GString;

import org.springframework.http.HttpStatus;


class BadRequestException extends CampaignException 
{
	public BadRequestException(String message) {
		super(message, HttpStatus.BAD_REQUEST);
	}

}
