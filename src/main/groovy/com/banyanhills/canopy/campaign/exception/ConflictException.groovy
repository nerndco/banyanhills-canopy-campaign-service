package com.banyanhills.canopy.campaign.exception

import org.springframework.http.HttpStatus

class ConflictException extends CampaignException 
{
	public ConflictException(String message) {
		super(message, HttpStatus.CONFLICT);
	}
}
