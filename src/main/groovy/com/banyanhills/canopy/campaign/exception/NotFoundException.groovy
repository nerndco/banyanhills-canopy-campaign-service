package com.banyanhills.canopy.campaign.exception

import groovy.lang.GString;
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

class NotFoundException extends CampaignException 
{
	public NotFoundException(String message) {
		super(message, HttpStatus.NOT_FOUND);
	}
}
