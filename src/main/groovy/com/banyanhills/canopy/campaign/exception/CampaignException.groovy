package com.banyanhills.canopy.campaign.exception

import groovy.lang.GString;

import org.springframework.http.HttpStatus;

abstract class CampaignException extends RuntimeException  {
	final HttpStatus statusCode;
	
	public CampaignException(String message, HttpStatus statusCode) {
		super(message);
		this.statusCode = statusCode;
	}
	public CampaignException(GString message, HttpStatus statusCode) {
		super(message.toString());
		this.statusCode = statusCode;
	}
}
