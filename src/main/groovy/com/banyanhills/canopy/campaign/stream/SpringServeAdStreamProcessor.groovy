package com.banyanhills.canopy.campaign.stream

import groovy.util.logging.Slf4j

import org.apache.commons.lang3.StringUtils
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component

import com.banyanhills.canopy.campaign.domain.StreamRequest

@Component
@Slf4j
class SpringServeAdStreamProcessor extends VastStreamProcessor implements StreamProcessor
{
	@Override
	public VastAdResponse requestVastAdResponse(StreamRequest request) {
		String adUrl = request.segment.adUrl;
		Map parameters = request.parameters;
		parameters.each { name, value ->
			adUrl = StringUtils.replace(adUrl, "{" + name + "}", value);
		}
		
		URL requestUrl = new URL(adUrl);
		String response = requestUrl.getText();
		
		log.debug("SpringServe Response from ${adUrl} " + response);
		
		if (StringUtils.isEmpty(response)) {
			return null;
		}
		else {
			return processVast(response, request, false);
		}
	}
}
