package com.banyanhills.canopy.campaign.stream

import groovy.util.logging.Slf4j

import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.lang3.StringUtils
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.impl.client.HttpClients
import org.apache.http.impl.client.LaxRedirectStrategy
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate

import com.banyanhills.canopy.campaign.domain.RequestContent
import com.banyanhills.canopy.campaign.domain.Segment
import com.banyanhills.canopy.campaign.domain.StreamRequest
import com.banyanhills.canopy.campaign.domain.StreamRequestStatus
import com.banyanhills.canopy.campaign.repository.RequestContentRepository
import com.banyanhills.canopy.campaign.service.StreamService
import com.banyanhills.canopy.campaign.service.VideoProcessor

@Component
@Slf4j
abstract class VastStreamProcessor implements StreamProcessor
{
	public static final List SUPPORTED_MIME_TYPES = [
		'video/mp4'
	]
	
	Logger impressionLog = LoggerFactory.getLogger("impression");
	
	private static final RestTemplate restTemplate;
	
	static {
		final HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
		final HttpClient httpClient = HttpClientBuilder.create()
													   .setRedirectStrategy(new LaxRedirectStrategy())
													   .build();
		factory.setHttpClient(httpClient);
		restTemplate = new RestTemplate(factory);
	}
	
	
	@Autowired
	StreamService streamService
	
	@Autowired
	VideoProcessor videoProcessor;
	
	@Autowired
	RequestContentRepository requestContentRepo;
	
	@Value('${stream.directory}')
	String streamDirectory;
	
	@Value('${content.directory}')
	String contentDirectory;
	
	public abstract VastAdResponse requestVastAdResponse(StreamRequest request);
	
	@Override
	@Async("campaignTaskExecutor")
	public void delete(StreamRequest request)
	{
	}
	
	@Override
	File get(StreamRequest request) {
		String fileName;
		if (request.requestContent != null && request.requestContent.success) {
			fileName = request.requestContent._bid + "." + request.segment.mediaType.ext;
		}
		else if (StringUtils.isNotEmpty(request.segment.defaultFile)) {
			fileName = request.segment.defaultFile;
		}
		else {
			fileName = 0 + "." + request.segment.mediaType.ext;
		}
		return new File(streamDirectory + File.separator + fileName);
	}
	
	@Override
	@Async("campaignTaskExecutor")
	public void generate(StreamRequest request)
	{
		File outputFile;
		try {
			Segment segment = request.segment;
			int adLength = segment.length;
			
			VastAdResponse response = requestVastAdResponse(request);
			if (response == null || response.mediaUrl == null) {
				streamService.updateStreamRequestStatus(request._bid, StreamRequestStatus.Fail, null)
			}
			else {
				RequestContent requestContent = new RequestContent();
				requestContent.mediaUrl = response.mediaUrl.toString();
				requestContent.mediaUrlCheckSum = DigestUtils.md5Hex(requestContent.mediaUrl);
				requestContent.mediaType = segment.mediaType;
				
				InputStream inputStream = new BufferedInputStream(response.mediaUrl.openStream());
				//OutputStream mediaFileStream = new FileOutputStream(rawMediaFile);
				ByteArrayOutputStream rawMediaStream = new ByteArrayOutputStream();
				byte[] data = new byte[10240];
				int tempLength = 0;
				while ((tempLength = inputStream.read(data)) > 0) {
					rawMediaStream.write(data, 0, tempLength);
				}
				data = null;
				inputStream.close();
				byte[] rawMediaBytes = rawMediaStream.toByteArray();
				rawMediaStream.close();
				requestContent.mediaCheckSum = DigestUtils.md2Hex(rawMediaBytes);
				
				List<RequestContent> existingContents = requestContentRepo.findByMediaUrlCheckSumAndMediaCheckSumAndMediaTypeAndSuccess(requestContent.mediaUrlCheckSum, requestContent.mediaCheckSum, requestContent.mediaType, true);
				if (existingContents.size() > 0) {
					rawMediaBytes = null;
					requestContent = existingContents[0];
					streamService.updateStreamRequestStatus(request._bid, StreamRequestStatus.Ready, requestContent)
				}
				else {
					String tempRequestContent = UUID.randomUUID().toString()
					File rawMediaFile = new File(streamDirectory + File.separator + tempRequestContent);
					if (rawMediaFile.exists()) {
						rawMediaFile.delete();
					}
					rawMediaFile.setBytes(rawMediaBytes);
					rawMediaBytes = null;
					
					File tmpFile = new File(streamDirectory + File.separator + tempRequestContent + "-tmp." + segment.mediaType.ext);
					videoProcessor.clip(rawMediaFile, tmpFile, segment.mediaType.vcodec, segment.startSecond, segment.endSecond, null, 0, 0, segment.mediaType);
					
					if (rawMediaFile.exists()) {
						rawMediaFile.delete();
					}
					
					if (tmpFile.exists()) {
						requestContent.success = true;
						requestContentRepo.save(requestContent);
						outputFile = new File(streamDirectory + File.separator + requestContent._bid + "." + segment.mediaType.ext)
						outputFile.delete();
						tmpFile.renameTo(outputFile.getAbsolutePath());
						streamService.updateStreamRequestStatus(request._bid, StreamRequestStatus.Ready, requestContent)
					}
					else {
						streamService.updateStreamRequestStatus(request._bid, StreamRequestStatus.Fail, null)
					}
				}
			}
			
			if (response != null) {
				sendImpression(segment, response);
			}
		}
		catch(Exception e) {
			log.error("Generate stream fail for ${request.requestId}", e);
			streamService.updateStreamRequestStatus(request._bid, StreamRequestStatus.Fail, null);
		}
	}
	
	protected sendImpression(Segment segment, VastAdResponse response) {
		URL impressionUrl = response.getImpressionUrl();
		if (impressionUrl != null) 
		{
            impressionLog.info("Sending ${segment.adType} impression for segment ${segment._bid}: " + impressionUrl);
            try  {
				restTemplate.getForObject(impressionUrl.toURI(), String.class);
				//CloseableHttpClient httpClient = HttpClients.createDefault()
                //httpClient.execute(new HttpGet(impressionUrl.toURI()));
            } catch (Exception ex) {
                impressionLog.info("Problem calling impression url: " + impressionUrl, ex);
            }
		} 
		else if (response.getProofOfPlayUrls().length > 0) 
		{
			for (URL impression : response.getProofOfPlayUrls()) {
				impressionLog.info("Sending ${segment.adType} impression for segment ${segment._bid}: " + impression);
				try  {
					restTemplate.getForObject(impression.toURI(), String.class);
					//CloseableHttpClient httpClient = HttpClients.createDefault()
					//httpClient.execute(new HttpGet(impression.toURI()));
				} catch (Exception ex) {
					impressionLog.info("Problem calling impression url: " + impression, ex);
				}
			}
		}
	}

	
	VastAdResponse processVast(String adm, StreamRequest request, boolean decode = true) {
		
		log.debug("Vast XML is: " + adm);
		
		XmlSlurper xmlSlurper = new XmlSlurper()
		if (decode) {
			adm = URLDecoder.decode(adm, 'UTF-8')
		}
		def xml = xmlSlurper.parseText(adm)
		
		VastAdResponse adResponse = new VastAdResponse(
			requestId: request.requestId,
			proofOfPlayUrls: []
		);

		try {
			adResponse.adId = xml.Ad.@id.text() as Long
			log.debug("Found Ad ID: ${adResponse.adId}")
		} catch (NumberFormatException e) {
			adResponse.adId = Math.floor(Math.random() * 1000)
			log.debug("Setting a random ad ID, this must be a test ad. Adid: ${adResponse.adId}")
		}

		def proofOfPlayUrl = ((xml.Ad.Wrapper.isEmpty() ? xml.Ad.InLine : xml.Ad.Wrapper).Creatives.Creative[0].Linear.TrackingEvents.Tracking.find {
			it.@event.text() == 'complete'
		}.text())

		log.debug("Found proof of play url: ${proofOfPlayUrl}")

		if (StringUtils.isNotBlank(proofOfPlayUrl)) {
			log.debug("Setting proof of play URL")
			adResponse.proofOfPlayUrls = adResponse.proofOfPlayUrls + new URL(proofOfPlayUrl)
		}

		log.debug("Adding impression urls to list of proof of play urls")

		(xml.Ad.Wrapper.isEmpty() ? xml.Ad.InLine : xml.Ad.Wrapper).Impression.each {
			if (it.text()) adResponse.proofOfPlayUrls = adResponse.proofOfPlayUrls + new URL(it.text())
		}

		log.debug("Current pop urls: ${adResponse.proofOfPlayUrls}")

		def mediaFile
		Integer mediaHeight
		Integer mediaWidth
		Integer mediaBitrate
		xml.Ad.InLine.isEmpty() ? null : xml.Ad.InLine.Creatives.Creative[0].Linear.MediaFiles.MediaFile.each {
			if (isSupportedType(it.@type.text())) {
				String bitrateDef = it.@bitrate.text();
				int bitrate = 0;
				if (StringUtils.isNotEmpty(bitrateDef) && bitrateDef.isNumber()) {
					 bitrate = bitrateDef as Integer;
				}
				if (mediaBitrate == null || mediaBitrate < bitrate) {
					mediaFile = it.text()
					mediaHeight = it.@height.text() as Integer
					mediaWidth = it.@width.text() as Integer
					mediaBitrate = bitrate
				}
			}
		}
		log.debug("MediaFile = ${mediaFile}")

		def mediaUrl = mediaFile ? new URL(mediaFile) : null
		log.debug("MediaURL = ${mediaUrl}")

		def vastAdTagUri = xml.Ad?.Wrapper?.VASTAdTagURI?.text()
		if (vastAdTagUri) {
			String result = restTemplate.getForObject(vastAdTagUri, String.class);
			
			if (StringUtils.isNotEmpty(result)) {
				VastAdResponse subsequentAdResponse = processVast(result, request)
				if (subsequentAdResponse != null) {
					adResponse.proofOfPlayUrls = adResponse.proofOfPlayUrls + subsequentAdResponse.proofOfPlayUrls
					adResponse.mediaUrl = subsequentAdResponse.mediaUrl
					adResponse.mediaHeight = subsequentAdResponse.mediaHeight
					adResponse.mediaWidth = subsequentAdResponse.mediaWidth
				}
			}
		} else {
			adResponse.mediaUrl = mediaUrl
			adResponse.mediaHeight = mediaHeight ? mediaHeight as Integer : null;
			adResponse.mediaWidth = mediaWidth ? mediaWidth as Integer : null;
		}

		log.debug("Returning adResponse: ${adResponse}")

		return adResponse
	}
	
	protected boolean isSupportedType(String type) {
		return SUPPORTED_MIME_TYPES.contains(type)
	}
		
}
