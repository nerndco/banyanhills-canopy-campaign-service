package com.banyanhills.canopy.campaign.stream

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component

import com.banyanhills.canopy.campaign.domain.Content
import com.banyanhills.canopy.campaign.domain.Segment
import com.banyanhills.canopy.campaign.domain.StreamRequest
import com.banyanhills.canopy.campaign.domain.StreamRequestStatus
import com.banyanhills.canopy.campaign.service.StreamService
import com.banyanhills.canopy.campaign.service.VideoProcessor

@Component
class VideoStreamProcessor implements StreamProcessor
{
	@Value('${segment.directory}')
	String segmentDirectory;
	
	@Value('${content.directory}')
	String contentDirectory;
	
	@Autowired
	VideoProcessor videoProcessor;
	
	@Autowired
	StreamService streamService

	@Override
	@Async("campaignTaskExecutor")
	public void generate(StreamRequest request) 
	{
		try {
			Segment segment = request.segment;
			Content content = segment.content;
			
			File contentFile = new File(contentDirectory + File.separator + content._bid);
			if (contentFile.exists()) {
				File outputFile = new File(segmentDirectory + File.separator + segment._bid + "." + segment.mediaType.ext)
				if (!outputFile.exists() || outputFile.size() == 0) {
					videoProcessor.clip(contentFile, outputFile, segment.mediaType.vcodec, segment.startSecond, segment.endSecond, segment.lengthSeconds, content.width, content.height, segment.mediaType);
				}
				if (request._bid != null) {
					streamService.updateStreamRequestStatus(request._bid, StreamRequestStatus.Ready)
				}
			}
			else {
				if (request._bid != null) {
					streamService.updateStreamRequestStatus(request._bid, StreamRequestStatus.Ready)
				}
			}
		}
		catch(Exception e) {
			if (request._bid != null) {
				streamService.updateStreamRequestStatus(request._bid, StreamRequestStatus.Fail)
			}
		}
	}
	
	@Override
	@Async("campaignTaskExecutor")
	public void delete(StreamRequest request)
	{
		Segment segment = request.segment;
		File outputFile = new File(segmentDirectory + File.separator + segment._bid + "." + segment.mediaType.ext)
		if (outputFile.exists()) {
			outputFile.delete();
		}
	}
	
	@Override
	File get(StreamRequest request) {
		Segment segment = request.segment;
		File outputFile = new File(segmentDirectory + File.separator + segment._bid + "." + segment.mediaType.ext)
		return outputFile;
	}
	

}
