package com.banyanhills.canopy.campaign.stream

import groovy.transform.ToString

@ToString(includeNames = true, includeFields = true)
class VastAdResponse {
    String requestId

    Long adId

    URL[] proofOfPlayUrls
    URL impressionUrl
    URL mediaUrl
	Integer mediaWidth
	Integer mediaHeight
}