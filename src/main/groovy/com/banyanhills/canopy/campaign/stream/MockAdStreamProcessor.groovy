package com.banyanhills.canopy.campaign.stream


import java.io.File;
import java.util.concurrent.ThreadLocalRandom

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component

import com.banyanhills.canopy.campaign.domain.Segment
import com.banyanhills.canopy.campaign.domain.StreamRequest
import com.banyanhills.canopy.campaign.domain.StreamRequestStatus
import com.banyanhills.canopy.campaign.service.StreamService
import com.banyanhills.canopy.campaign.service.VideoProcessor

@Component
class MockAdStreamProcessor implements StreamProcessor
{
	@Value('${stream.directory}')
	String streamDirectory;
	
	@Value('${content.directory}')
	String contentDirectory;
	
	@Autowired
	VideoProcessor videoProcessor;

	@Autowired
	StreamService streamService
	
	@Override
	@Async("campaignTaskExecutor")
	public void generate(StreamRequest request)
	{
		Segment segment = request.segment;
		File videoFolder = new File(request.segment.adUrl);
		File[] videoFiles = videoFolder.listFiles();
		
		int randomNum = ThreadLocalRandom.current().nextInt(0, videoFiles.length);
		File videoFile = videoFiles[randomNum];
		File outputFile = new File(streamDirectory + File.separator + request.requestId + "." + segment.mediaType.ext)
		videoProcessor.clip(videoFile, outputFile, segment.mediaType.vcodec, 0, null, null, 0, 0, segment.mediaType);
		streamService.updateStreamRequestStatus(request._bid, StreamRequestStatus.Ready)
	}
	
	@Override
	@Async("campaignTaskExecutor")
	public void delete(StreamRequest request)
	{
		
	}
	
	@Override
	File get(StreamRequest request) {
		String fileName = request.requestId + "." + request.segment.mediaType.ext;
		File file = new File(streamDirectory + File.separator + fileName);
	}
	

}
