package com.banyanhills.canopy.campaign.stream

import groovy.util.logging.Slf4j

import java.security.MessageDigest

import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.http.client.SimpleClientHttpRequestFactory
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate

import com.banyanhills.canopy.campaign.domain.Segment
import com.banyanhills.canopy.campaign.domain.StreamRequest

@Component
@Slf4j
class RubiconAdStreamProcessor extends VastStreamProcessor implements StreamProcessor
{
	@Value('${rubicon.url}')
	String url
	
	@Value('${rubicon.username}')
	String username

	@Value('${rubicon.password}')
	String password

	@Value('${rubicon.size_id}')
	int size_id;
	
	@Value('${rubicon.zone_id}')
	int zone_id;
	
	@Value('${rubicon.account_id}')
	int account_id
	
	@Value('${rubicon.site_id}')
	int site_id
	
	@Value('${rubicon.site_name}')
	String site_name;
	
	@Value('${rubicon.site_domain}')
	String site_domain;
	
	@Value('${rubicon.site_page}')
	String site_page
	
	@Value('${rubicon.site_agent}')
	String site_agent

	@Value('${rubicon.site_ip}')
	String site_ip

	@Value('${rubicon.site_zip}')
	String site_zip
	
	private static final RestTemplate restTemplate;
	
	static {
		SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
		restTemplate = new RestTemplate(requestFactory);
	}
	
	private Map parseParameter(Map requestParameters, String adUrl) {
		Map parameters = [:];
		if (StringUtils.isNotEmpty(adUrl)) {
			String[] paramValues = StringUtils.split(adUrl, "&");
			paramValues.each { String param ->
				if (StringUtils.isNotEmpty(param)) {
					String[] values = StringUtils.split(param, "=");
					if (values.length == 2) {
						parameters.put(values[0], values[1]);
					}
				}
			}
		}
		parameters.putAll(requestParameters);
		return parameters;
	}
	
	@Override
	public VastAdResponse requestVastAdResponse(StreamRequest request) 
	{
		Segment segment = request.segment;
		Map parameters = parseParameter(request.parameters, request.segment.adUrl);
		
		String macsha1 = "";
		if (StringUtils.isNotEmpty(parameters.mac)) {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			md.update(parameters.mac.getBytes());
			BigInteger hash = new BigInteger(1, md.digest());
			macsha1 = hash.toString(16);
		}
		
		def rubiconRequest = [
			id:request.requestId,
			imp: [
				[
					id:request.requestId,
					video: [
						mimes: SUPPORTED_MIME_TYPES,
						minduration:15,
						maxduration:segment.length,
						playbackmethod:[1,3],
						boxingallowed:0,
						ext: [
							skip:0,
							rp: [
								size_id:parameters.size_id ?: size_id
							]
						]
					],
					ext: [
						rp: [
							zone_id:parameters.zone_id ?: zone_id
						]
					]
				]
			],
			site: [
				name:parameters.site_name ?: site_name,
				domain:parameters.site_domain ?: site_domain,
				page:parameters.site_page ?: site_page,
				publisher: [
					ext: [
						rp: [
							account_id:parameters.account_id ?: account_id
						]
					]
				],
				ext: [
					rp: [
						site_id:parameters.site_id ?: site_id
					]
				]
			],
			device: [
				ip: parameters.site_ip ?: site_ip,
				ua: parameters.site_agent ?: site_agent,
				geo: [
					zip: parameters.site_zip ?: site_zip
				],
				macsha1: macsha1
			]
		]
		
		log.debug("Rubicon Request + " + rubiconRequest);
		
		String auth = username + ":" + password;
		String authEncodeString = Base64.encoder.encodeToString(auth.getBytes("US-ASCII"));
		
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.add("Authorization", "Basic ${authEncodeString}");
		
		HttpEntity<?> httpEntity = new HttpEntity<Object>(rubiconRequest, requestHeaders);
		ResponseEntity<Map> response = restTemplate.exchange(url, HttpMethod.POST, httpEntity, Map.class);
		
		Map rubiconResponse = response.body;
		log.debug("Rubicon Response + " + rubiconResponse);
		
		String adm = rubiconResponse.seatbid?.find { true }?.bid?.find { true }?.adm;
		
		if (StringUtils.isEmpty(adm)) {
			return null;
		}
		else {
			return processVast(adm, request);
		}

	} 
}
