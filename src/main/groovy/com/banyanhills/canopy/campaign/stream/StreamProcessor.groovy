package com.banyanhills.canopy.campaign.stream

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import com.banyanhills.canopy.campaign.domain.Segment
import com.banyanhills.canopy.campaign.domain.StreamRequest


interface StreamProcessor 
{
	void generate(StreamRequest request);
	
	void delete(StreamRequest request);
	
	File get(StreamRequest request);
	
	
}
