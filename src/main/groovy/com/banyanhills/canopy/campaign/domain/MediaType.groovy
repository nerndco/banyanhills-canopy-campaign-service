package com.banyanhills.canopy.campaign.domain

enum MediaType 
{
	mp4("libx264", "mp4"), 
	wmv("msmpeg4", "wmv"),
	wmv2("wmv2", "wmv"),
	wmv3("wmv2", "wmv")
	
	String vcodec
	String ext
	
	MediaType(String vcodec, String ext) {
		this.vcodec = vcodec
		this.ext = ext;
	}
	
}
