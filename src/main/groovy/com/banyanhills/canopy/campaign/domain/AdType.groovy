package com.banyanhills.canopy.campaign.domain

enum AdType {
	Rubicon("rubiconAdStreamProcessor"), 
	SpringServe("springServeAdStreamProcessor"),
	Mock("mockAdStreamProcessor")
	
	
	String processorBean
	
	AdType(String processorBean) {
		this.processorBean = processorBean;
	}
}
