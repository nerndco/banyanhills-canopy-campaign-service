package com.banyanhills.canopy.campaign.domain

class Manifest {
	String name
	Long timestamp
	List<ManifestLine> manifestLines;
}
