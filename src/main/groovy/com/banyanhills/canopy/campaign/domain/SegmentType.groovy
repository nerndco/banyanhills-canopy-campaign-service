package com.banyanhills.canopy.campaign.domain

enum SegmentType {
	Content, 
	Ad
}
