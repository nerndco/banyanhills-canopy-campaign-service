package com.banyanhills.canopy.campaign.domain

import groovy.transform.ToString

import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.Transient

import org.apache.commons.lang3.StringUtils

import com.banyanhills.canopy.campaign.service.CampaignService
import com.banyanhills.canopy.campaign.service.ContentService
import com.banyanhills.meta.annotation.MetaColumn
import com.banyanhills.tenant.TenantEntity
import com.fasterxml.jackson.annotation.JsonIgnore

@Entity
@Table(name = "segment")
@ToString(includeNames = true, includeFields = true)
class Segment extends TenantEntity  {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaColumn(title = "Natvie ID", type = "Number", order = 0, hidden = true)
	Long _bid
	
    @Enumerated(EnumType.STRING)
	SegmentType type
	
    @Enumerated(EnumType.STRING)
	AdType adType
	
	String adUrl;

    @Enumerated(EnumType.STRING)
	MediaType mediaType
		
	int sortOrder	
	Integer repeatTimes
	Integer startSecond
	Integer endSecond
	Integer lengthSeconds
	Integer requestInterval
	
	String defaultFile;
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "campaign_bid")
	Campaign campaign
	
	@Transient
	public Long getCampaign_bid() {
		return campaign?._bid;
	}
	
	@Transient
	public void setCampaign_bid(Long id) {
		campaign = CampaignService.instance.get(id);
	}

	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "content_bid")
	Content content
	
	@Transient
	public Long getContent_bid() {
		return content?._bid;
	}
	
	@Transient
	public void setContent_bid(Long id) {
		content = ContentService.instance.get(id);
	}
	
	@Transient
	@JsonIgnore
	public int getLength() {
		if (lengthSeconds != null) {
			return lengthSeconds;
		}
		else if (endSecond != null && startSecond != null) {
			return endSecond - startSecond + 1
		}
		else if (endSecond != null) {
			return endSecond + 1;
		}
		else {
			return 0;
		}

	}

	public MediaType getMediaType() {
		mediaType != null ? mediaType : MediaType.wmv;
	}
}
