package com.banyanhills.canopy.campaign.domain

import groovy.transform.ToString

import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.PrePersist
import javax.persistence.PreRemove
import javax.persistence.PreUpdate
import javax.persistence.Table
import javax.persistence.Transient

import org.apache.commons.lang3.StringUtils

import com.banyanhills.tenant.MultiTenancyHolder
import com.fasterxml.jackson.annotation.JsonIgnore

@Entity
@Table(name = "stream_request")
@ToString(includeNames = true, includeFields = true)
class StreamRequest {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long _bid
	
	String requestId
	
	@Transient
	boolean success = true;
	
    @Enumerated(EnumType.STRING)
	StreamRequestStatus status
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "segment_bid")
	@JsonIgnore
	Segment segment
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "request_content_bid")
	@JsonIgnore
	RequestContent requestContent
	
	@Transient
	@JsonIgnore
	Map parameters;
	
	@Transient
	String remoteLocation
	
	@Transient
	Long repeatInterval;
	
	@Transient
	String repeatLocation;
	
	Date created;
	Date updated;

	String stream
	String host
	String ip
	String mac
	
	@PreUpdate
	private void onEntityModification() throws Exception {
		updated = new Date();
		if (created == null) created = updated;
	}
	
	@PrePersist
	private void onPersist() {
		created = new Date();
		updated = created;
	}
}
