package com.banyanhills.canopy.campaign.domain;

import groovy.transform.ToString

import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.Table

import com.banyanhills.meta.annotation.MetaColumn
import com.banyanhills.meta.annotation.MetaDefaultSortable
import com.banyanhills.tenant.TenantEntity
import com.fasterxml.jackson.annotation.JsonIgnore

@Entity
@Table(name = "campaign")
@ToString(includeNames = true, includeFields = true)
class Campaign extends TenantEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaColumn(title = "Natvie ID", type = "Number", order = 0, hidden = true)
	Long _bid
		
	@MetaColumn(title = "Campaign ID", type = "String", order = 1, sortable = true, creatable = true, updatable = true, searchable = true, defaultSortable = @MetaDefaultSortable(direction = "asc"))
	String campaignId;
	
	@MetaColumn(title = "Name", type = "String", order = 1, sortable = true, creatable = true, updatable = true, searchable = true, defaultSortable = @MetaDefaultSortable(direction = "asc"))
	String name;
	
	@MetaColumn(title = "Description", type = "String", order = 2, sortable = true, creatable = true, updatable = true, searchable = true)
	String description;
	
	@MetaColumn(title = "Start Date", type = "Date", order = 3, sortable = true, creatable = true, updatable = true, searchable = true)
	Date startDate;
	
	@MetaColumn(title = "End Date", type = "Date", order = 4, sortable = true, creatable = true, updatable = true, searchable = true)
	Date endDate;

	@OneToMany(mappedBy = "campaign", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonIgnore
	List<Segment> segments
	
	@JsonIgnore
	public long getLengthSeconds() {
		if (segments != null) {
			return segments?.sum { it.lengthSeconds };
		}
		else {
			return 0;
		}
	}
	
}
