package com.banyanhills.canopy.campaign.domain

import groovy.transform.ToString

import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.PrePersist
import javax.persistence.PreUpdate
import javax.persistence.Table

import com.banyanhills.meta.annotation.MetaColumn

@Entity
@Table(name = "request_content")
@ToString(includeNames = true, includeFields = true)
class RequestContent {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaColumn(title = "ID", type = "Number", order = 0, hidden = true)
	Long _bid
	
	String mediaUrl
	String mediaUrlCheckSum
	String mediaCheckSum
	
	@Enumerated(EnumType.STRING)
	MediaType mediaType
	
	boolean success
	
	Date created;
	Date updated;
	
	@PreUpdate
	private void onEntityModification() throws Exception {
		updated = new Date();
		if (created == null) created = updated;
	}
	
	@PrePersist
	private void onPersist() {
		created = new Date();
		updated = created;
	}
}
