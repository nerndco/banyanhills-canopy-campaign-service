package com.banyanhills.canopy.campaign.domain

enum StreamRequestStatus {
	Processing, Fail, Ready, Downloaded, Played
}
