package com.banyanhills.canopy.campaign.domain

import groovy.transform.ToString

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table
import javax.persistence.Transient

import org.apache.commons.lang3.StringUtils

import com.banyanhills.canopy.campaign.service.ContentService
import com.banyanhills.meta.annotation.MetaColumn
import com.banyanhills.tenant.TenantEntity
import com.fasterxml.jackson.annotation.JsonIgnore

@Entity
@Table(name = "content")
@ToString(includeNames = true, includeFields = true)
class Content extends TenantEntity
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaColumn(title = "ID", type = "Number", order = 0, hidden = true)
	Long _bid

	String name;
	String description;	
	String contentType
	String fileName
	String ext
	String checksum
	Long size
	Long width
	Long height
	Long thumbnailWidth
	Long thumbnailHeight
	Long duration
	
	@Transient
	public String getThumbnailFileName() {
		return _bid + "_thumbnail." + (isVideo() ? "jpg" : ext);
	}
	
	@Transient
	public String getThumbnailContentType() {
		return isVideo() ? "image/jpeg" : contentType;
	}
	
	@Transient
	public String getThumbnail() {
		return ContentService.instance.getThumbnailLink(this);
	}
	
	@Transient
	public String getContent() {
		return isVideo() ? ContentService.instance.getStreamLink(this) : ContentService.instance.getImageLink(this);
	}
	
	@Transient
	public boolean isVideo() {
		return contentType.startsWith("video");
	}
	
	@Transient
	@JsonIgnore
	public boolean isValid() {
		if (StringUtils.isEmpty(ext)) {
			return false;
		}
		
		if (isVideo()) {
			if (!(ext.toUpperCase() in ['MOV', 'MP4'])) {
				return false;
			}
		}
		else {
			if (!(ext.toUpperCase() in ['GIF', 'PNG', 'JPG'])) {
				return false;
			} 
		}
		
		return true;
	}
}
