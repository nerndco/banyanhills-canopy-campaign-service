package com.banyanhills.canopy.campaign.controller

import groovy.util.logging.Slf4j

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

import com.banyanhills.canopy.campaign.domain.Campaign
import com.banyanhills.canopy.campaign.service.CampaignService
import com.banyanhills.canopy.service.util.search.SearchCriteria
import com.banyanhills.meta.annotation.EnableCanopyMeta
import com.banyanhills.meta.annotation.Meta
import com.banyanhills.meta.annotation.MetaExtension
import com.banyanhills.meta.annotation.MetaRowAction
import com.banyanhills.tenant.TenantEntityController

@RestController
@Slf4j
@RequestMapping("/v1/campaigns")
@EnableCanopyMeta
class CampaignController extends TenantEntityController 
{
	@Autowired
	CampaignService campaignService
	
	@RequestMapping(value = "/all", method = [RequestMethod.GET, RequestMethod.POST])
	@Meta(metaClass = Campaign.class, title = "Campaigns", create = true, update = true, delete = true, map = false, pivot = false, segment = false,
			rowAction = [
					@MetaRowAction(title = "UPDATE", icon = "icon2-pencil", action = "/v1/campaigns/", method = RequestMethod.PUT),
					@MetaRowAction(title = "DELETE", icon = "icon1-delete2", action = "/v1/campaigns/", method = RequestMethod.DELETE)
			])
	@MetaExtension
	Page<Campaign> getAll(
			Pageable pageable,
			@RequestParam(value = "filter", required = false) String filter,
			@RequestBody(required = false) Map postSearchCriteria) {
		SearchCriteria searchCriteria = postSearchCriteria ? new SearchCriteria(postSearchCriteria) : new SearchCriteria(filter)
		return campaignService.search(pageable, searchCriteria)
	}
			
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	Campaign getById(@PathVariable Long id) {
		return campaignService.get(id)
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	List<Campaign> byIdsGet(@RequestParam List<Long> idList) {
		return campaignService.getFromList(idList)
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	List<Campaign> byIdsPost(@RequestBody List<Long> idList) {
		return campaignService.getFromList(idList)
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	Campaign create(@RequestBody Campaign campaign) {
		return campaignService.create(campaign)
	}

	@RequestMapping(value = "/", method = RequestMethod.PUT)
	Campaign update(@RequestBody Campaign campaign) {
		return campaignService.update(campaign)
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	void deleteById(@PathVariable Long id) {
		campaignService.delete(id)
	}
	
	@RequestMapping(value = "/status", method = RequestMethod.GET)
	def status() {
		return campaignService.status()
	}
	
			
}
