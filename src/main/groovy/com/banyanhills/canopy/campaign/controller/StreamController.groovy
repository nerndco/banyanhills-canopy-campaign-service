package com.banyanhills.canopy.campaign.controller

import groovy.util.logging.Slf4j

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

import com.banyanhills.canopy.campaign.domain.StreamRequest
import com.banyanhills.canopy.campaign.service.StreamService

@RestController
@Slf4j
@RequestMapping("/v1/campaigns/segments/stream")
class StreamController {
	@Autowired
	StreamService streamService
	
	@RequestMapping(value = "/request/{id}", method = RequestMethod.GET)
	StreamRequest requestStream(
		@PathVariable Long id,
		@RequestParam(required=false) String requestId,
		@RequestParam(required=false) String stream,
		@RequestParam(required=false) String host,
		@RequestParam(required=false) String ip,
		@RequestParam(required=false) String mac,
		HttpServletRequest request) {
		return  streamService.requestStream(id, requestId, stream, host, ip, mac, request);
	}
		
	@RequestMapping(value = "/playback/{requestId}", method = RequestMethod.GET)
	void playback(
		@PathVariable String requestId,
		HttpServletRequest request,
		HttpServletResponse response) {
		streamService.playback(requestId, request, response);
	}
		
	@RequestMapping(value = "/download/{requestId}", method = RequestMethod.GET)
	void download(
		@PathVariable String requestId,
		HttpServletRequest request,
		HttpServletResponse response) {
		streamService.download(requestId, request, response);
	}
}
