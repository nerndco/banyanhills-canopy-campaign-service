package com.banyanhills.canopy.campaign.controller

import groovy.util.logging.Slf4j

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

import com.banyanhills.canopy.campaign.domain.Manifest
import com.banyanhills.canopy.campaign.service.ManifestService

@RestController
@Slf4j
@RequestMapping("/v1/campaigns/manifest")
class ManifestController {
	
	@Autowired
	ManifestService manifestService
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	Manifest getById(
		@PathVariable String id,
		@RequestParam(required=false) Long timestamp) 
	{
		return manifestService.get(id, timestamp)
	}
}
