package com.banyanhills.canopy.campaign.controller

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import groovy.util.logging.Slf4j

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

import com.banyanhills.canopy.campaign.domain.MediaType
import com.banyanhills.canopy.campaign.domain.MoveRequest
import com.banyanhills.canopy.campaign.domain.Segment
import com.banyanhills.canopy.campaign.service.SegmentService

@RestController
@Slf4j
@RequestMapping("/v1/campaigns/segments")
class SegmentController {
	@Autowired
	SegmentService segmentService
	
	@RequestMapping(value = "/mediaTypes", method = RequestMethod.GET)
	MediaType[] getMediaTypes() {
		return MediaType.values();
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	List<Segment> getAll(
			@RequestParam(value = "campaign_bid") Long campaign_bid) {
		return segmentService.getAll(campaign_bid)
	}
			
	@RequestMapping(value = "/", method = RequestMethod.POST)
	Segment create(@RequestBody Segment segment) {
		return segmentService.create(segment)
	}
	
	@RequestMapping(value = "/", method = RequestMethod.PUT)
	Segment update(@RequestBody Segment segment) {
		return segmentService.update(segment)
	}
	
	@RequestMapping(value = "/move", method = RequestMethod.PUT)
	void move(@RequestBody MoveRequest request) {
		segmentService.move(request.segments)
	}
	
	@RequestMapping(value = "/moveUp/{id}", method = RequestMethod.PUT)
	void moveUp(@PathVariable Long id) {
		segmentService.moveUp(id)
	}
	
	@RequestMapping(value = "/moveDown/{id}", method = RequestMethod.PUT)
	void moveDown(@PathVariable Long id) {
		segmentService.moveDown(id)
	}
	
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	void deleteById(@PathVariable Long id) {
		segmentService.delete(id)
	}
}
