package com.banyanhills.canopy.campaign.controller

import groovy.util.logging.Slf4j

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile

import com.banyanhills.canopy.campaign.domain.Campaign
import com.banyanhills.canopy.campaign.domain.Content
import com.banyanhills.canopy.campaign.exception.NotFoundException
import com.banyanhills.canopy.campaign.service.ContentService
import com.banyanhills.canopy.service.util.search.SearchCriteria
import com.banyanhills.meta.annotation.Meta
import com.banyanhills.meta.annotation.MetaExtension
import com.banyanhills.meta.annotation.MetaRowAction

@RestController
@Slf4j
@RequestMapping("/v1/contents")
class ContentController {

	@Autowired
	ContentService contentService
	
	@RequestMapping(value = "/all", method = [RequestMethod.GET, RequestMethod.POST])
	@Meta(metaClass = Campaign.class, title = "Campaigns", create = true, update = true, delete = true, map = false, pivot = false, segment = false,
			rowAction = [
					@MetaRowAction(title = "UPDATE", icon = "icon2-pencil", action = "/v1/campaigns/", method = RequestMethod.PUT),
					@MetaRowAction(title = "DELETE", icon = "icon1-delete2", action = "/v1/campaigns/", method = RequestMethod.DELETE)
			])
	@MetaExtension
	Page<Content> getAll(
			Pageable pageable,
			@RequestParam(value = "filter", required = false) String filter,
			@RequestBody(required = false) Map postSearchCriteria) {
		SearchCriteria searchCriteria = postSearchCriteria ? new SearchCriteria(postSearchCriteria) : new SearchCriteria(filter)
		return contentService.search(pageable, searchCriteria)
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	@ResponseBody
	public Content handleFileUpload(HttpServletRequest request,
			@RequestParam("file") MultipartFile file) 
	{
		return contentService.save(file);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.PUT)
	Content update(@RequestBody Content content) {
		return contentService.update(content)
	}
	
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	void deleteById(@PathVariable Long id) {
		contentService.delete(id)
	}
	
	@RequestMapping(value = "/image/thumbnail/{id}", method = RequestMethod.GET)
	ResponseEntity<byte[]> getThumbnailImage(@PathVariable Long id) {
		ResponseEntity<byte[]> response = contentService.getThumbnail(id);
		if (response == null) {
			throw new NotFoundException("Content not existing: ${id}");
		}
		
		return response;
	}
	
	@RequestMapping(value = "/image/{id}", method = RequestMethod.GET)
	ResponseEntity<byte[]> getImage(@PathVariable Long id) {
		ResponseEntity<byte[]> response = contentService.getImage(id);
		if (response == null) {
			throw new NotFoundException("Content not existing: ${id}");
		}
		
		return response;
	}
	
	@RequestMapping(value = "/stream/{id}", method = RequestMethod.GET)
	void stream(
		@PathVariable Long id,
		HttpServletRequest request,
		HttpServletResponse response) {
		contentService.stream(id, request, response);
	}
		
	@RequestMapping(value = "/download/{id}", method = RequestMethod.GET)
	void download(
		@PathVariable Long id,
		HttpServletRequest request,
		HttpServletResponse response) {
		contentService.download(id, request, response);
	}
}
